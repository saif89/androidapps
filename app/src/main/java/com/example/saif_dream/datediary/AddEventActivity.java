package com.example.saif_dream.datediary;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddEventActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner eventDateTypeSp;
    EditText personNameET, eventDateET, eventTimeET, eventNoteET, eventContactET, eventMessageET;
    DatePickerDialog eventDatePickerDialog;
    TimePickerDialog timePickerDialog;
    private SimpleDateFormat dateFormatter;
    CheckBox sendSMSCheck, giveNotificationCheck;
    private String eventDateType, personName, eventDate, eventTime, eventNote, eventContact, sendSMS,
            eventMessage, giveNotification = "";

    Button saveEvent, clearEvent, backToTheListButton;
    DateDiary dateDiary;
    DateDiaryController dateDiaryController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        eventDateTypeSp = (Spinner) findViewById(R.id.dropDownList);
        eventDateTypeSp.requestFocus();

        dateFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        eventDateET = (EditText) findViewById(R.id.eventDateET);
        eventDateET.setInputType(InputType.TYPE_NULL);
        eventDateET.setOnClickListener(this);

        eventTimeET = (EditText) findViewById(R.id.eventTimeET);
        eventTimeET.setInputType(InputType.TYPE_NULL);
        eventTimeET.setOnClickListener(this);

        eventDateTypeSp.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        personNameET = (EditText) findViewById(R.id.personNameET);
        eventNoteET = (EditText) findViewById(R.id.eventNoteET);
        eventContactET = (EditText) findViewById(R.id.eventContactET);

        sendSMSCheck = (CheckBox) findViewById(R.id.isSendSMSET);
        eventMessageET = (EditText) findViewById(R.id.messageET);

        saveEvent = (Button) findViewById(R.id.saveEvent);
        saveEvent.setOnClickListener(this);

        clearEvent = (Button) findViewById(R.id.clearField);
        clearEvent.setOnClickListener(this);

        backToTheListButton = (Button) findViewById(R.id.backToTheListButton);
        backToTheListButton.setOnClickListener(this);

        giveNotificationCheck = (CheckBox) findViewById(R.id.notificationET);
        dateDiaryController = new DateDiaryController(AddEventActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.eventDateET : {
                Calendar newCalendar = Calendar.getInstance();
                eventDatePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                eventDateET.setText(dateFormatter.format(newDate.getTime()));
                            }
                        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                eventDatePickerDialog.show();
            }
            break;

            case R.id.eventTimeET : {
                Calendar newCalendar = Calendar.getInstance();
                timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                eventTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
            break;

            case R.id.saveEvent : {
                eventDateType = eventDateTypeSp.getSelectedItem().toString();
                personName = personNameET.getText().toString();
                eventDate = eventDateET.getText().toString();
                eventTime = eventTimeET.getText().toString();
                //Toast.makeText(getApplicationContext(), eventDate, Toast.LENGTH_LONG).show();
                if(eventDateType.equals("Choose a Type")){
                    Toast.makeText(getApplicationContext(), "Please Select A Date Type..", Toast.LENGTH_LONG).show();
                    return;
                }
                eventNote = eventNoteET.getText().toString();
                eventContact = eventContactET.getText().toString();
                eventMessage = eventMessageET.getText().toString();
                if(sendSMSCheck.isChecked()){
                    sendSMS = "Yes";
                } else sendSMS = "No";

                if(giveNotificationCheck.isChecked()){
                    giveNotification = "Yes";
                } else giveNotification = "No";

                dateDiary = new DateDiary(eventDateType, personName, eventDate, eventTime, eventNote,
                        eventContact, sendSMS, eventMessage, giveNotification);
                boolean isInserted = dateDiaryController.addDateEvent(dateDiary);
                if (isInserted) {
                    Toast.makeText(getApplicationContext(), "Save Successfully", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Insertion Failed", Toast.LENGTH_LONG).show();
                }
                clear();
            }
            break;

            case R.id.clearField : {
                clear();
            }
            break;

            case R.id.backToTheListButton : {
                Intent backToListIntent = new Intent(AddEventActivity.this, MainActivity.class);
                startActivity(backToListIntent);
            }
            break;
        }
    }

    public void clear(){
        eventDateType = "";
        personName = "";
        eventDate = "";
        eventTime = "";
        eventNote = "";
        eventContact = "";
        sendSMS = "";
        eventMessage = "";
        giveNotification = "";
        eventDateTypeSp.setSelection(0);
        personNameET.setText(personName);
        eventDateET.setText(eventDate);
        eventTimeET.setText(eventTime);
        eventNoteET.setText(eventNote);
        eventContactET.setText(eventContact);
        sendSMSCheck.setChecked(false);
        eventMessageET.setText(sendSMS);
        giveNotificationCheck.setChecked(false);
    }
}
