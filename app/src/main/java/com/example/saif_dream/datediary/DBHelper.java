package com.example.saif_dream.datediary;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;

/**
 * Created by saif-dream on 3/17/2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    static final int DATABASE_VERSION = 1;
    private static final String TAG = "DBHelper";
    static final String DATABASE_NAME="date_diary_db";

    static final String TABLE_DATE_DIARY="date_diary_event";
    static final String COL_EVENT_ID ="event_id";
    static final String COL_EVENT_DATE_TYPE ="event_date_type";
    static final String COL_PERSON_NAME="person_name";
    static final String COL_EVENT_DATE ="event_date";
    static final String COL_EVENT_TIME ="event_time";
    static final String COL_EVENT_NOTE ="event_note";
    static final String COL_EVENT_CONTACT ="event_contact";
    static final String COL_EVENT_MESSAGE ="event_message";
    static final String COL_IS_SEND_SMS = "is_send_sms";
    static final String COL_IS_GIVE_NOTIFICATION = "is_give_notification";

    private static final String CREATE_TABLE_DATE_DIARY=" CREATE TABLE " + TABLE_DATE_DIARY +
            " ( " + COL_EVENT_ID + " INTEGER PRIMARY KEY," + COL_EVENT_DATE_TYPE + " TEXT,"
                + COL_PERSON_NAME + " TEXT," + COL_EVENT_DATE + " TEXT,"  + COL_EVENT_TIME + " TEXT,"
                + COL_EVENT_NOTE + " TEXT,"+ COL_EVENT_CONTACT + " TEXT," + COL_EVENT_MESSAGE + " TEXT "
                + COL_IS_SEND_SMS + " TEXT " +   COL_IS_GIVE_NOTIFICATION  + " TEXT" +
            ") ";

    private static final String DATABASE_ALTER_COL_IS_SEND_SMS = "ALTER TABLE "
            + TABLE_DATE_DIARY + " ADD COLUMN " + COL_IS_SEND_SMS + " TEXT";

    private static final String DATABASE_ALTER_COL_IS_GIVE_NOTIFICATION = "ALTER TABLE "
            + TABLE_DATE_DIARY + " ADD COLUMN " + COL_IS_GIVE_NOTIFICATION + " TEXT";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_DATE_DIARY);
        //db.execSQL(DATABASE_ALTER_COL_IS_SEND_SMS);
        //db.execSQL(DATABASE_ALTER_COL_IS_GIVE_NOTIFICATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all data");
        //db.execSQL("DROP TABLE IF EXISTS "+ TABLE_DATE_DIARY);
        /*if (oldVersion == 2) {
            db.execSQL(DATABASE_ALTER_COL_IS_SEND_SMS);
            db.execSQL(DATABASE_ALTER_COL_IS_GIVE_NOTIFICATION);
        }*/
        /*if (oldVersion < 3) {
            db.execSQL(DATABASE_ALTER_COL_IS_GIVE_NOTIFICATION);
        }*/
        //onCreate(db);
    }
}
