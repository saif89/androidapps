package com.example.saif_dream.datediary;

/**
 * Created by saif-dream on 3/17/2016.
 */
public class DateDiary {
    private Integer eventId;
    private String eventDateType;
    private String personName;
    private String eventDate;
    private String eventTime;
    private String eventNote;
    private String contactNo;
    private String eventMessage;
    private String isSendSMS;
    private String isGiveNotification;

    public DateDiary(Integer eventId, String eventDateType, String personName, String eventDate,
                     String eventTime, String eventNote, String contactNo, String eventMessage,
                     String isSendSMS, String isGiveNotification) {
        this(eventDateType, personName, eventDate, eventTime, eventNote, contactNo, eventMessage,
                isSendSMS, isGiveNotification);
        this.eventId = eventId;
    }

    public DateDiary(String eventDateType, String personName, String eventDate, String eventTime,
                     String eventNote, String contactNo, String eventMessage, String isSendSMS,
                     String isGiveNotification) {
        this(eventDateType, personName, eventDate, eventTime, eventNote, isSendSMS, isGiveNotification);
        this.contactNo = contactNo;
        this.eventMessage = eventMessage;
    }

    public DateDiary(String eventDateType, String personName, String eventDate, String eventTime,
                     String eventNote, String isSendSMS, String isGiveNotification) {
        this.eventDateType = eventDateType;
        this.personName = personName;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventNote = eventNote;
        this.isSendSMS = isSendSMS;
        this.isGiveNotification = isGiveNotification;
    }

    public DateDiary() {}

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventDateType() {
        return eventDateType;
    }

    public void setEventDateType(String eventDateType) {
        this.eventDateType = eventDateType;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getIsSendSMS() {
        return isSendSMS;
    }

    public void setIsSendSMS(String isSendSMS) {
        this.isSendSMS = isSendSMS;
    }

    public String getIsGiveNotification() {
        return isGiveNotification;
    }

    public void setIsGiveNotification(String isGiveNotification) {
        this.isGiveNotification = isGiveNotification;
    }
}
