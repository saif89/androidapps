package com.example.saif_dream.datediary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by saif-dream on 3/20/2016.
 */
public class DateDiaryAdapter extends ArrayAdapter<DateDiary> {
    ArrayList<DateDiary> dateDiaryArrayListList;
    Context context;
    TextView eventDateTypeTV, eventDateTV;

    public DateDiaryAdapter(Context context, ArrayList<DateDiary> dateDiaryArrayListList) {
        super(context, R.layout.grid_item, dateDiaryArrayListList);
        this.dateDiaryArrayListList = dateDiaryArrayListList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.grid_item, null);

        eventDateTypeTV = (TextView) convertView.findViewById(R.id.eventDateTypeTV);
        eventDateTV = (TextView) convertView.findViewById(R.id.eventDateTV);

        eventDateTypeTV.setText(dateDiaryArrayListList.get(position).getEventDateType());
        eventDateTV.setText(dateDiaryArrayListList.get(position).getEventDate());

        return convertView;
    }
}
