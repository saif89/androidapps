package com.example.saif_dream.datediary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by saif-dream on 3/17/2016.
 */
public class DateDiaryController {
    private DateDiary dateDiary;
    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public DateDiaryController(Context context) { dbHelper = new DBHelper(context); }

    private void open() { db = dbHelper.getWritableDatabase(); }

    private void close() { dbHelper.close(); }

    public boolean addDateEvent(DateDiary dateDiary) {
        this.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.COL_EVENT_DATE_TYPE, dateDiary.getEventDateType());
        contentValues.put(DBHelper.COL_PERSON_NAME, dateDiary.getPersonName());
        contentValues.put(DBHelper.COL_EVENT_DATE, dateDiary.getEventDate());
        contentValues.put(DBHelper.COL_EVENT_TIME, dateDiary.getEventTime());
        contentValues.put(DBHelper.COL_EVENT_NOTE, dateDiary.getEventNote());
        contentValues.put(DBHelper.COL_EVENT_CONTACT, dateDiary.getContactNo());
        contentValues.put(DBHelper.COL_EVENT_MESSAGE, dateDiary.getEventMessage());
        contentValues.put(DBHelper.COL_IS_SEND_SMS, dateDiary.getIsSendSMS());
        contentValues.put(DBHelper.COL_IS_GIVE_NOTIFICATION, dateDiary.getIsGiveNotification());

        long inserted = db.insert(DBHelper.TABLE_DATE_DIARY, null, contentValues);
        this.close();

        if (inserted > 0) {
            return true;
        } else return false;
    }

    public DateDiary getDateEvent(int id) {
        this.open();
        Cursor cursor = db.query(
                            DBHelper.TABLE_DATE_DIARY, new String[]{DBHelper.COL_EVENT_ID,
                            DBHelper.COL_EVENT_DATE_TYPE, DBHelper.COL_PERSON_NAME, DBHelper.COL_EVENT_DATE,
                            DBHelper.COL_EVENT_TIME, DBHelper.COL_EVENT_NOTE, DBHelper.COL_EVENT_CONTACT,
                            DBHelper.COL_EVENT_MESSAGE, DBHelper.COL_IS_SEND_SMS, DBHelper.COL_IS_GIVE_NOTIFICATION},
                            DBHelper.COL_EVENT_ID + "= " + id, null, null, null, null
                        );

        cursor.moveToFirst();

        int mid = cursor.getInt(cursor.getColumnIndex(DBHelper.COL_EVENT_ID));
        String eventDateType = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_DATE_TYPE));
        String personName = cursor.getString(cursor.getColumnIndex(DBHelper.COL_PERSON_NAME));
        String eventDate = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_DATE));
        String eventTime = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_TIME));
        String eventNote = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_NOTE));
        String contactNo = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_CONTACT));
        String eventMessage = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_MESSAGE));
        String isSendSMS = cursor.getString(cursor.getColumnIndex(DBHelper.COL_IS_SEND_SMS));
        String isGiveNotification = cursor.getString(cursor.getColumnIndex(DBHelper.COL_IS_GIVE_NOTIFICATION));

        dateDiary = new DateDiary(mid, eventDateType, personName, eventDate, eventTime, eventNote,
                contactNo, eventMessage, isSendSMS, isGiveNotification);
        this.close();

        return dateDiary;
    }

    public ArrayList<DateDiary> getAllDateEvent() {
        this.open();
        ArrayList<DateDiary> dateEventList = new ArrayList<>();
        Cursor cursor = db.query(DBHelper.TABLE_DATE_DIARY, null, null, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                int mid = cursor.getInt(cursor.getColumnIndex(DBHelper.COL_EVENT_ID));
                String eventDateType = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_DATE_TYPE));
                String personName = cursor.getString(cursor.getColumnIndex(DBHelper.COL_PERSON_NAME));
                String eventDate = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_DATE));
                String eventTime = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_TIME));
                String eventNote = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_NOTE));
                String contactNo = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_CONTACT));
                String eventMessage = cursor.getString(cursor.getColumnIndex(DBHelper.COL_EVENT_MESSAGE));
                String isSendSMS = cursor.getString(cursor.getColumnIndex(DBHelper.COL_IS_SEND_SMS));
                String isGiveNotification = cursor.getString(cursor.getColumnIndex(DBHelper.COL_IS_GIVE_NOTIFICATION));

                dateDiary = new DateDiary(mid, eventDateType, personName, eventDate, eventTime,
                        eventNote, contactNo, eventMessage, isSendSMS, isGiveNotification);
                dateEventList.add(dateDiary);
                cursor.moveToNext();
            }
        }
        this.close();
        return dateEventList;
    }

    public boolean updateDateEvent(int id, DateDiary dateDiary) {
        this.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.COL_EVENT_DATE_TYPE, dateDiary.getEventDateType());
        contentValues.put(DBHelper.COL_PERSON_NAME, dateDiary.getPersonName());
        contentValues.put(DBHelper.COL_EVENT_DATE, dateDiary.getEventDate());
        contentValues.put(DBHelper.COL_EVENT_TIME, dateDiary.getEventTime());
        contentValues.put(DBHelper.COL_EVENT_NOTE, dateDiary.getEventNote());
        contentValues.put(DBHelper.COL_EVENT_CONTACT, dateDiary.getContactNo());
        contentValues.put(DBHelper.COL_EVENT_MESSAGE, dateDiary.getEventMessage());
        contentValues.put(DBHelper.COL_IS_SEND_SMS, dateDiary.getIsSendSMS());
        contentValues.put(DBHelper.COL_IS_GIVE_NOTIFICATION, dateDiary.getIsGiveNotification());

        int updated = db.update(DBHelper.TABLE_DATE_DIARY, contentValues, DBHelper.COL_EVENT_ID + " = " + id, null);
        this.close();
        if (updated > 0) {
            return true;
        } else return false;
    }

    public boolean removeDateEvent(int id) {
        this.open();
        int deleted = db.delete(DBHelper.TABLE_DATE_DIARY, DBHelper.COL_EVENT_ID + " = " + id, null);
        this.close();
        if (deleted > 0) {
            return true;
        } else return false;
    }
}
