package com.example.saif_dream.datediary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ListView listView;
    DateDiary dateDiary;
    DateDiaryController dateDiaryController;
    DateDiaryAdapter dateDiaryAdapter;
    ArrayList<DateDiary> dateDiaryArrayList;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<String> arrayList;

    /*DateDiaryAdapter birthDayListAdapter, marriageDayListAdapter, deathDayListAdapter,
            meetingDayListAdapter, othersDayListAdapter;*/

    //GridView gridViewByBirth, gridViewByMarriage, gridViewByDeath, gridViewByMeeting, gridViewByOthers;
    //ArrayList<DateDiary> dateDiaries, birthDayList, marriageDayList, deathDayList, meetingDayList, othersDayList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView = (GridView) findViewById(R.id.gridView);
        dateDiaryController = new DateDiaryController(this);
        dateDiaryArrayList = new ArrayList<>();
        dateDiaryArrayList = dateDiaryController.getAllDateEvent();

        dateDiaryAdapter = new DateDiaryAdapter(this, dateDiaryArrayList);
        gridView.setAdapter(dateDiaryAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent singleEventIntent = new Intent(MainActivity.this, SingleDateEventActivity.class);
                //int clickedItem = dateDiaryArrayList.get(position).getEventId();

                Bundle bundle = new Bundle();
                bundle.putString("id",String.valueOf(dateDiaryArrayList.get(position).getEventId()));
                bundle.putString("dateType",dateDiaryArrayList.get(position).getEventDateType());
                bundle.putString("person",dateDiaryArrayList.get(position).getPersonName());
                bundle.putString("date",dateDiaryArrayList.get(position).getEventDate());
                bundle.putString("time",dateDiaryArrayList.get(position).getEventTime());
                bundle.putString("note",dateDiaryArrayList.get(position).getEventNote());
                bundle.putString("contact",dateDiaryArrayList.get(position).getContactNo());
                bundle.putString("isSendSMS",dateDiaryArrayList.get(position).getIsSendSMS());
                bundle.putString("message",dateDiaryArrayList.get(position).getEventMessage());
                bundle.putString("isSendNotification", dateDiaryArrayList.get(position).getIsGiveNotification());
                singleEventIntent.putExtra("eventBundle", bundle);

                startActivity(singleEventIntent);
            }
        });

        /*gridViewByBirth = (GridView) findViewById(R.id.gridViewByBirth);
        gridViewByMarriage = (GridView) findViewById(R.id.gridViewByMarriage);
        gridViewByDeath = (GridView) findViewById(R.id.gridViewByMeeting);
        gridViewByMeeting = (GridView) findViewById(R.id.gridViewByDeath);
        gridViewByOthers = (GridView) findViewById(R.id.gridViewByOthers);*/

        /*birthDayList = new ArrayList<DateDiary>();
        marriageDayList = new ArrayList<DateDiary>();
        deathDayList = new ArrayList<DateDiary>();
        meetingDayList = new ArrayList<DateDiary>();
        othersDayList = new ArrayList<DateDiary>();*/

        //getAllEvent();
    }

    public  void getAllEvent(){

        /*birthDayListAdapter = new DateDiaryAdapter(this, dateDiaries);
        marriageDayListAdapter = new DateDiaryAdapter(this, marriageDayList);
        meetingDayListAdapter = new DateDiaryAdapter(this, meetingDayList);
        deathDayListAdapter = new DateDiaryAdapter(this, deathDayList);
        othersDayListAdapter = new DateDiaryAdapter(this, othersDayList);*/


        /*for(DateDiary dateDiary : dateDiaries){
            if (dateDiary.getEventDateType().equals("Birth Day")){
                birthDayList.add(dateDiary);
                //birthDayListAdapter = new DateDiaryAdapter(this, birthDayList);
                gridViewByBirth.setAdapter(birthDayListAdapter);
            }
            if (dateDiary.getEventDateType().equals("Marriage Day")){
                marriageDayList.add(dateDiary);
                //marriageDayListAdapter = new DateDiaryAdapter(this, marriageDayList);
                gridViewByMarriage.setAdapter(marriageDayListAdapter);
            }
            if (dateDiary.getEventDateType().equals("Meeting Day")){
                meetingDayList.add(dateDiary);
                //meetingDayListAdapter = new DateDiaryAdapter(this, meetingDayList);
                gridViewByMeeting.setAdapter(meetingDayListAdapter);
            }
            if (dateDiary.getEventDateType().equals("Death Day")){
                deathDayList.add(dateDiary);
                //deathDayListAdapter = new DateDiaryAdapter(this, deathDayList);
                gridViewByDeath.setAdapter(deathDayListAdapter);
            }
            if (dateDiary.getEventDateType().equals("Others")){
                othersDayList.add(dateDiary);
                //othersDayListAdapter = new DateDiaryAdapter(this, othersDayList);
                gridViewByOthers.setAdapter(othersDayListAdapter);
            }
        }

        if(birthDayList.isEmpty()){
            gridViewByBirth.setVisibility(View.GONE);
        }
        if (marriageDayList.isEmpty()){
            gridViewByMarriage.setVisibility(View.GONE);
        }
        if(deathDayList.isEmpty()){
            gridViewByMeeting.setVisibility(View.GONE);
        }
        if (marriageDayList.isEmpty()){
            gridViewByMarriage.setVisibility(View.GONE);
        }
        if (othersDayList.isEmpty()){
            gridViewByOthers.setVisibility(View.GONE);
        }*/

        //Toast.makeText(getApplicationContext(),dateDiary.getPersonName(),Toast.LENGTH_LONG).show();
    }

    public void goAddEventActivity(View view){
        Intent addEventIntent = new Intent(MainActivity.this, AddEventActivity.class);
        startActivity(addEventIntent);
    }
}
