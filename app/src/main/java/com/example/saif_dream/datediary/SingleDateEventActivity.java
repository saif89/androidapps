package com.example.saif_dream.datediary;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SingleDateEventActivity extends AppCompatActivity implements View.OnClickListener  {
    Bundle bundle;
    Spinner eventDateTypeSp;
    EditText personNameET, eventDateET, eventTimeET, eventNoteET, eventContactET, eventMessageET;
    DatePickerDialog eventDatePickerDialog;
    TimePickerDialog timePickerDialog;
    private SimpleDateFormat dateFormatter;
    CheckBox sendSMSCheck, giveNotificationCheck;

    String eventDateType, personName, eventDate, eventTime, eventNote, eventContact, sendSMS,
            eventMessage, giveNotification = "";
    String emptyString = "";

    Button updateButton, removeButton, backToTheListButton;
    String id;

    DateDiary dateDiary;
    DateDiaryController dateDiaryController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_date_event);

        dateDiary = new DateDiary();
        dateDiaryController = new DateDiaryController(this);
        dateFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

        eventDateTypeSp = (Spinner) findViewById(R.id.dropDownList);

        eventDateET = (EditText) findViewById(R.id.eventDateET);
        eventDateET.setOnClickListener(this);

        eventTimeET = (EditText) findViewById(R.id.eventTimeET);
        eventTimeET.setInputType(InputType.TYPE_NULL);
        eventTimeET.setOnClickListener(this);

        eventDateTypeSp.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        personNameET = (EditText) findViewById(R.id.personNameET);
        eventNoteET = (EditText) findViewById(R.id.eventNoteET);
        eventContactET = (EditText) findViewById(R.id.eventContactET);

        sendSMSCheck = (CheckBox) findViewById(R.id.isSendSMSET);
        eventMessageET = (EditText) findViewById(R.id.messageET);
        giveNotificationCheck = (CheckBox) findViewById(R.id.notificationET);

        backToTheListButton = (Button) findViewById(R.id.backToTheListButton);
        backToTheListButton.setOnClickListener(this);


        updateButton = (Button) findViewById(R.id.updateEventButton);
        updateButton.setOnClickListener(this);

        removeButton = (Button) findViewById(R.id.removeEventButton);
        removeButton.setOnClickListener(this);

        bundle = getIntent().getBundleExtra("eventBundle");
        Toast.makeText(SingleDateEventActivity.this, bundle.getString("date"), Toast.LENGTH_LONG).show();
        setEvent();
    }

    public void setEvent(){
        if(bundle.getString("date") == "" || bundle.getString("date") == null ){
            eventDateET.setText(emptyString);
        } else {
            eventDateET.setText(bundle.getString("date"));
        }

        if(bundle.getString("time") == "" || bundle.getString("time") == null ){
            eventTimeET.setText(emptyString);
        } else {
            eventTimeET.setText(bundle.getString("time"));
        }

        if(bundle.getString("person") == "" || bundle.getString("person") == null ){
            personNameET.setText(emptyString);
        } else {
            personNameET.setText(bundle.getString("person"));
        }

        if(bundle.getString("note") == "" || bundle.getString("note") == null ){
            eventNoteET.setText(emptyString);
        }else {
            eventNoteET.setText(bundle.getString("note"));
        }

        if(bundle.getString("contact") == "" || bundle.getString("contact") == null ){
            eventContactET.setText(emptyString);
        }else {
            eventContactET.setText(bundle.getString("contact"));
        }

        if(bundle.getString("isSendSMS") == "" || bundle.getString("isSendSMS") == null ){
            sendSMSCheck.setChecked(false);
        }else {
            sendSMSCheck.setChecked(true);
        }

        if(bundle.getString("message") == "" || bundle.getString("message") == null ){
            eventMessageET.setText(emptyString);
        }else {
            eventMessageET.setText(bundle.getString("message"));
        }

        if(bundle.getString("isSendNotification") == "" || bundle.getString("isSendNotification") == null ){
            giveNotificationCheck.setChecked(false);
        }else {
            giveNotificationCheck.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.eventDateET : {
                Calendar newCalendar = Calendar.getInstance();
                eventDatePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                eventDateET.setText(dateFormatter.format(newDate.getTime()));
                            }
                        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                eventDatePickerDialog.show();
            }
            break;

            case R.id.eventTimeET : {
                Calendar newCalendar = Calendar.getInstance();
                timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                eventTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
            break;
            case R.id.updateEventButton : {
                eventDateType = eventDateTypeSp.getSelectedItem().toString();
                personName = personNameET.getText().toString();
                eventDate = eventDateET.getText().toString();
                eventTime = eventTimeET.getText().toString();
                Toast.makeText(getApplicationContext(), personName, Toast.LENGTH_LONG).show();
                if(eventDateType.equals("Choose a Type")){
                    Toast.makeText(getApplicationContext(), "Please Select A Date Type..", Toast.LENGTH_LONG).show();
                    return;
                }
                eventNote = eventNoteET.getText().toString();
                eventContact = eventContactET.getText().toString();
                eventMessage = eventMessageET.getText().toString();
                if(sendSMSCheck.isChecked()){
                    sendSMS = "Yes";
                } else sendSMS = "No";

                if(giveNotificationCheck.isChecked()){
                    giveNotification = "Yes";
                } else giveNotification = "No";

                dateDiary = new DateDiary(eventDateType, personName, eventDate, eventTime, eventNote,
                        eventContact, sendSMS, eventMessage, giveNotification);
                boolean isUpdated = dateDiaryController.updateDateEvent(Integer.parseInt(bundle.getString("id"))-1, dateDiary);

                if(isUpdated){
                    if (isUpdated) {
                        Toast.makeText(getApplicationContext(), "Update Successfully", Toast.LENGTH_LONG).show();
                        goBackToTheList();
                    } else {
                        Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;

            case R.id.removeEventButton : {
                Toast.makeText(getApplicationContext(), "Removed is called", Toast.LENGTH_LONG).show();
                boolean isRemove = dateDiaryController.removeDateEvent(Integer.parseInt(bundle.getString("id"))-1);
                if(isRemove){
                    if (isRemove) {
                        Toast.makeText(getApplicationContext(), "Event Removed.", Toast.LENGTH_LONG).show();
                        goBackToTheList();
                    } else {
                        Toast.makeText(getApplicationContext(), "Remove Failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;

            case R.id.backToTheListButton : {
                goBackToTheList();
            }
            break;
        }
    }
    private void goBackToTheList(){
        Intent backToListIntent = new Intent(SingleDateEventActivity.this, MainActivity.class);
        startActivity(backToListIntent);
    }
}
